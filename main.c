#include <stdio.h>
#include <strings.h>

#include "program_config.h"

int main(int argc, char *argv[])
{
    struct program_config config;
    bzero(&config, sizeof config);

    program_config_init(&config, argc, argv);
    program_config_inspect(&config);

    if(program_is_ready(&config))
        program_exec(&config);
}
