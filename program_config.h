#ifndef H_PROGRAM_CONFIG_CAPUCIN
#	define H_PROGRAM_CONFIG_CAPUCIN

#	include <grp.h>
#	include <pwd.h>
#	include <stdbool.h>

struct program_config
{
    char * user;
	uid_t uid;
    char * group;
	gid_t gid;

	struct {
		char **argv;
		int argc;
	} command;
};

void program_exec(struct program_config * config);
bool program_is_ready(struct program_config * config);
void program_config_inspect(struct program_config * config);
void program_config_init(struct program_config * config, int argc, char *argv[]);
void program_config_set_group(struct program_config * config, char * group);
void program_config_set_user(struct program_config * config, char * user);

#endif // H_PROGRAM_CONFIG_CAPUCIN
