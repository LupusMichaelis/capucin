#include <errno.h>
#include <grp.h>
#include <pwd.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/types.h>
#include <unistd.h>

#include "program_config.h"

bool program_is_ready(struct program_config * config)
{
	if(config->uid < 0)
	{
		fprintf(stderr, "Uid wrong\n");
		return false;
	}

	if(config->gid < 0)
	{
		fprintf(stderr, "Gid wrong\n");
		return false;
	}

	if(0 == config->command.argc)
	{
		fprintf(stderr, "No command given\n");
		return false;
	}

	return true;
}

void program_exec(struct program_config * config)
{
	switch(setgid(config->gid))
	{
		case EINVAL:
			fprintf(stderr, "setgid: not valid gid (%s:%d)\n", config->group, config->gid);
			return;
		case EPERM:
			fprintf(stderr, "setgid: unauthorized or gid not belonging (%s:%d)\n", config->group, config->gid);
			return;
		case -1:
			fprintf(stderr, "setuid: can't set gid (%d): %s\n", config->gid, strerror(errno));
	}

	switch(setuid(config->uid))
	{
		case EAGAIN:
			fprintf(stderr, "setuid: uid not matching real uid, kernel failure (%d)\n", config->uid);
			return;
		case EINVAL:
			fprintf(stderr, "setuid: not valid uid (%d)\n", config->uid);
			return;
		case EPERM:
			fprintf(stderr, "setuid: not privileged uid (%d)\n", config->uid);
			return;
		case -1:
			fprintf(stderr, "setuid: can't set uid (%d): %s\n", config->uid, strerror(errno));
	}

	if(0 > execvp(config->command.argv[0], config->command.argv))
		switch(errno)
		{
			case ENOENT:
				fprintf(stderr, "Exe not found: %s\n", config->command.argv[0]);
				break;
			default:
				perror("execvp");
		}
}

void program_config_inspect(struct program_config * config)
{
    printf
        (
            "user: %s (%d)\n"
            "group: %s (%d)\n"
        , config->user, config->uid
        , config->group, config->gid
        );

	int argc = config->command.argc;
	printf("command: [");
	for(int i=0; i < argc; ++i)
		printf(i != (argc-1) ? "\"%s\", ": "\"%s\"", config->command.argv[i]);
	printf("]\n");
}

void program_config_init(struct program_config * config, int argc, char *argv[])
{
	config->uid = -1;
	config->gid = -1;

    do
    {
        int opt = getopt(argc, argv, "u:g:");

        if(-1 == opt)
            break;

        switch(opt)
        {
            case 'u':
                program_config_set_user(config, (char *) optarg);
                break;
            case 'g':
                program_config_set_group(config, (char *) optarg);
                break;
            default:
                fprintf(stderr, "Usage: %s [ -u user ] [ -g group ]\n", argv[0]);
        }
    }
    while(true);

	config->command.argv = &argv[optind];
	config->command.argc = argc - optind;
}

void program_config_set_user(struct program_config * config, char * user)
{
    int size = (1 + strlen(user)) * sizeof *user;
    config->user = realloc(config->user, size);
    bzero(config->user, size);
    strcpy(config->user, user);

	struct passwd * entry = getpwnam(config->user);
	if(entry)
		config->uid = entry->pw_uid;
}

void program_config_set_group(struct program_config * config, char * group)
{
    int size = (1 + strlen(group)) * sizeof *group;
    config->group = realloc(config->group, size);
    bzero(config->group, size);
    strcpy(config->group, group);

	struct group * entry = getgrnam(config->group);
	if(entry)
		config->gid = entry->gr_gid;
}
