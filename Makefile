CFLAGS= \
	-Wall \
	-pedantic \
	-g -ggdb

SRCS=$(wildcard *.c)
OBJS=$(SRCS:.c=.o)

.PHONY: clean

capucin: $(OBJS)
	$(CC) -o capucin $(OBJS)

clean:
	- rm -rf capucin $(OBJS)
